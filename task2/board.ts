const getChess = (cellsInRow: number) => {
    let cell: string = '';
    for (let i = 0; i < cellsInRow; i++) {
        for (let j = 0; j < cellsInRow; j++) {
            if (i % 2 === 0) {
                if((j % 2) === 1 ) {
                    cell += '  ';
                }else {
                    cell += '██';
                }
            } else {
                if((j % 2) === 1 ) {
                    cell += '██';
                }else {
                    cell += '  ';
                }
            }
        }
        cell += '\n';
    }
    return cell;
};
console.log(getChess(8));